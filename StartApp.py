import sys, os
from PyQt5 import QtWidgets, uic, QtGui
from StartProgram import StartProgram

def main():
    app = QtWidgets.QApplication(sys.argv)
    app.setWindowIcon(QtGui.QIcon('LeanPlayground/assets/favicon_2.ico'))
    lp = StartProgram()

    sys.exit(app.exec_())

if __name__ == "__main__":
    main()