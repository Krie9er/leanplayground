import gettext
import os

localdir = os.path.join(os.path.abspath(os.path.dirname(__file__)), 'LeanPlayground\languages')
gettext.install('tryOut',localdir)
print(localdir)
setLang = "de"


def greet():
    'Prints out greeting message.'
    age = 25
    print(_('Hi'))
    print(_("What's up?"))
    print(_('I am {age} years old!').format(age=20))
    print('\n')

def select_language(language,localdir):
    lang = gettext.translation('tryOut',localdir,languages=[language],fallback=True)
    lang.install('tryOut')

greet()

select_language('de',localdir)
greet()

select_language('en',localdir)
greet()