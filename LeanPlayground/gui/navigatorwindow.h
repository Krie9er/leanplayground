#ifndef NAVIGATORWINDOW_H
#define NAVIGATORWINDOW_H

#include <QMainWindow>

QT_BEGIN_NAMESPACE
namespace Ui { class NavigatorWindow; }
QT_END_NAMESPACE

class NavigatorWindow : public QMainWindow
{
    Q_OBJECT

public:
    NavigatorWindow(QWidget *parent = nullptr);
    ~NavigatorWindow();

private:
    Ui::NavigatorWindow *ui;
};
#endif // NAVIGATORWINDOW_H
