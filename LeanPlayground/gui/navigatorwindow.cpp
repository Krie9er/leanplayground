#include "navigatorwindow.h"
#include "ui_navigatorwindow.h"

NavigatorWindow::NavigatorWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::NavigatorWindow)
{
    ui->setupUi(this);
}

NavigatorWindow::~NavigatorWindow()
{
    delete ui;
}

