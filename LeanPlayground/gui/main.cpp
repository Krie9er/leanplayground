#include "navigatorwindow.h"

#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    NavigatorWindow w;
    w.show();
    return a.exec();
}
