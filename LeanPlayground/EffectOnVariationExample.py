from PyQt5 import QtWidgets, uic, QtGui
from PyQt5.QtGui import QIcon, QPixmap
from PyQt5.QtCore import Qt, QThread, pyqtSignal
from PartSimulation import *

import time
import random

class  PlayEOVE(QtWidgets.QWidget):

    changed_speed = pyqtSignal(int)

    def __init__(self, main_window, parent=None):
        super().__init__(parent)

        self.simulation_active = False

        self.ui = uic.loadUi('LeanPlayground/gui/EffectOfVariationExample.ui', self)
        self.show()
        self.main_window = main_window
        self.loadAssets()
        self.reset_stats()

        # Stockhandler
        self.ui.imgStock2.setPixmap(QPixmap('LeanPlayground/assets/stock_empty.png'))
        self.ui.imgStock3.setPixmap(QPixmap('LeanPlayground/assets/stock_empty.png'))
        self.ui.imgFGStock.setPixmap(QPixmap('LeanPlayground/assets/stock_empty.png'))

        self.actions()


    def actions(self):
        # actions
        self.ui.btnResetSimulation.clicked.connect(self.reset_stats)
        self.ui.sbSpeed.valueChanged.connect(self.set_simulation_speed)
        self.ui.btnPlaySimulation.clicked.connect(self.play_simulation)
        self.ui.btnPauseSimulation.released.connect(self.pause_simulation)
        self.ui.timeSlider.valueChanged.connect(self.update_simulation_speed)


    def pause_simulation(self):
        print("Pause")
        self.ui.btnPauseSimulation.setEnabled(False)
        self.ui.btnPlaySimulation.setEnabled(True)
        if self.simulation_active:
            self.thread.terminate()
            self.simulation_active = False

    def closeEvent(self, event):
        self.main_window.show()

    def loadAssets(self):
        # machines
        self.ui.imgMachine1.setPixmap(QPixmap('LeanPlayground/assets/machine.png'))
        self.ui.imgMachine2.setPixmap(QPixmap('LeanPlayground/assets/machine.png'))
        self.ui.imgMachine3.setPixmap(QPixmap('LeanPlayground/assets/machine.png'))

        #Stocks
        self.ui.imgStock2.setPixmap(QPixmap('LeanPlayground/assets/stock_filled_full.png'))
        self.ui.imgStock3.setPixmap(QPixmap('LeanPlayground/assets/stock_filled_half.png'))
        self.ui.imgFGStock.setPixmap(QPixmap('LeanPlayground/assets/stock_empty.png'))

    def play_simulation(self):
        if not self.simulation_active:
            self.ui.btnPauseSimulation.setEnabled(True)
            self.ui.btnPlaySimulation.setEnabled(False)
            self.simulation_active = True
            self.thread = ProductionProcess(self.process_parameter, self)
            print('Aufruf')
            self.thread.process_loop.connect(self.write_to_frontend)
            self.thread.start()

    def test(self, val):
        print(val)

    def set_simulation_speed(self):
        if self.ui.sbSpeed.value() == self.ui.sbSpeed.maximum():
            self.simulation_speed = 0
            self.ui.dataTimeSlider.setText("Max. Calc.")
        else:
            self.simulation_speed = self.ui.sbSpeed.value()
            self.ui.dataTimeSlider.setText(str(int(self.simulation_speed)) + " x")
        self.ui.timeSlider.setValue(self.ui.sbSpeed.value())
        self.changed_speed.emit(self.simulation_speed)

    def update_simulation_speed(self):
        self.ui.sbSpeed.setValue(self.ui.timeSlider.value())

    def get_simulation_speed(self):
        return self.simulation_speed

    def reset_stats(self):

        #print('reset', self.simulation_active)

        #calculation Vars
        self.ui.btnPauseSimulation.setEnabled(False)
        self.ui.btnPlaySimulation.setEnabled(True)
        self.iteration = 0
        self.simulation_speed = self.ui.sbSpeed.value()
        self.process_parameter = {
            'stock_level_2_no_variation' : 0,
            'stock_level_3_no_variation' : 0,
            'stock_level_FG_no_variation' : 0,
            'stock_level_2_with_variation' : 0,
            'stock_level_3_with_variation' : 0,
            'stock_level_FG_with_variation' : 0,
            'machine_one_with_variation_lead_time' : 10,
            'machine_two_with_variation_lead_time' : 10,
            'machine_three_with_variation_lead_time': 10,
            'machine_no_variation_lead_time' : 10,
            'machine_one_with_variation_status': 0,
            'machine_two_with_variation_status' : 0,
            'machine_three_with_variation_status' : 0,
            'machine_one_no_variation_status' : 0,
            'machine_two_no_variation_status' : 0,
            'machine_three_no_variation_status' : 0,
            'iteration' : 0
        }

        if self.simulation_active:
            self.thread.terminate()
            self.simulation_active = False

        self.write_to_frontend()

    def write_to_frontend(self):
        self.ui.pgBarMachineOneNoVariation.setValue(self.process_parameter['machine_one_no_variation_status'])
        self.ui.pgBarMachineOneWithVariation.setValue(self.process_parameter['machine_one_with_variation_status'])
        self.ui.pgBarMachineThreeWithVariation.setValue(self.process_parameter['machine_three_with_variation_status'])
        self.ui.pgBarMachineTwoNoVariation.setValue(self.process_parameter['machine_two_no_variation_status'])
        self.ui.pgBarMachineTwoWithVariation.setValue(self.process_parameter['machine_two_with_variation_status'])
        self.ui.pgBarmachineThreeNoVariation.setValue(self.process_parameter['machine_three_no_variation_status'])

        # Fill Textfields
        self.ui.dataTime.setText(str(self.process_parameter['iteration']) + ' min')
        self.set_simulation_speed()
        self.ui.dataStockMachineTwoNoVariation.setText(str(self.process_parameter['stock_level_2_no_variation']))
        self.ui.dataStockMachineThreeNoVariation.setText(str(self.process_parameter['stock_level_3_no_variation']))
        self.ui.dataStockFinishedProductsNoVariation.setText(str(self.process_parameter['stock_level_FG_no_variation']))

        self.ui.dataStockMachineTwoWithVariation.setText(str(self.process_parameter['stock_level_2_with_variation']))
        self.ui.dataStockMachineThreeWithVariation.setText(str(self.process_parameter['stock_level_3_with_variation']))
        self.ui.dataStockFinishedProductsWithVariation.setText(str(self.process_parameter['stock_level_FG_with_variation']))



class ProductionProcess(QThread):

    process_loop = pyqtSignal(object)

    def __init__(self, frontend_com, window):
        QThread.__init__(self)
        self.window = window
        self.frontend_com = frontend_com
        self.speed = window.get_simulation_speed()
        self.window.changed_speed.connect(self.change_speed)
        print(self.speed)

    def run(self):
        # no variation
        machine_one_no_variation_lead_time = self.frontend_com['machine_no_variation_lead_time']
        machine_two_no_variation_lead_time = self.frontend_com['machine_no_variation_lead_time']
        machine_three_no_variation_lead_time = self.frontend_com['machine_no_variation_lead_time']

        # with variation
        variation_minutes = 9
        machine_one_with_variation_lead_time = self.get_lead_time(self.frontend_com['machine_one_with_variation_lead_time'], variation_minutes)
        machine_two_with_variation_lead_time = self.get_lead_time(self.frontend_com['machine_two_with_variation_lead_time'], variation_minutes)
        machine_three_with_variation_lead_time = self.get_lead_time(self.frontend_com['machine_three_with_variation_lead_time'], variation_minutes)

        # temp values
        temp1 = machine_one_with_variation_lead_time
        temp2 = machine_two_with_variation_lead_time
        temp3 = machine_three_with_variation_lead_time
        while True:
            self.frontend_com['iteration'] = self.frontend_com['iteration'] + 1
            # no variation
            ### machine 1 production
            if(machine_one_no_variation_lead_time is not 0):
                machine_one_no_variation_lead_time = machine_one_no_variation_lead_time - 1

            else:
                self.frontend_com['stock_level_2_no_variation'] = self.frontend_com['stock_level_2_no_variation'] + 1
                machine_one_no_variation_lead_time = self.frontend_com['machine_no_variation_lead_time']

            #### machine 2 production
            if(self.frontend_com['stock_level_2_no_variation'] is not 0):
                if (machine_two_no_variation_lead_time is not 0):
                    machine_two_no_variation_lead_time = machine_two_no_variation_lead_time - 1

                else:
                    self.frontend_com['stock_level_3_no_variation'] = self.frontend_com[
                                                                          'stock_level_3_no_variation'] + 1
                    self.frontend_com['stock_level_2_no_variation'] = self.frontend_com[
                                                                          'stock_level_2_no_variation'] - 1
                    machine_two_no_variation_lead_time = self.frontend_com['machine_no_variation_lead_time']

            #### machine 3 production
            if (self.frontend_com['stock_level_3_no_variation'] is not 0):
                if (machine_three_no_variation_lead_time is not 0):
                    machine_three_no_variation_lead_time = machine_three_no_variation_lead_time - 1

                else:
                    self.frontend_com['stock_level_FG_no_variation'] = self.frontend_com[
                                                                          'stock_level_FG_no_variation'] + 1
                    self.frontend_com['stock_level_3_no_variation'] = self.frontend_com[
                                                                          'stock_level_3_no_variation'] - 1
                    machine_three_no_variation_lead_time = self.frontend_com['machine_no_variation_lead_time']

            # with variation
            ### machine 1 production
            if (machine_one_with_variation_lead_time is not 0):
                machine_one_with_variation_lead_time = machine_one_with_variation_lead_time - 1

            else:
                self.frontend_com['stock_level_2_with_variation'] = self.frontend_com[
                                                                      'stock_level_2_with_variation'] + 1
                machine_one_with_variation_lead_time = self.get_lead_time(self.frontend_com['machine_one_with_variation_lead_time'], variation_minutes)
                temp1 = machine_one_with_variation_lead_time

            #### machine 2 production
            if (self.frontend_com['stock_level_2_with_variation'] is not 0):
                if (machine_two_with_variation_lead_time is not 0):
                    machine_two_with_variation_lead_time = machine_two_with_variation_lead_time - 1

                else:
                    self.frontend_com['stock_level_3_with_variation'] = self.frontend_com[
                                                                          'stock_level_3_with_variation'] + 1
                    self.frontend_com['stock_level_2_with_variation'] = self.frontend_com[
                                                                          'stock_level_2_with_variation'] - 1
                    machine_two_with_variation_lead_time = self.get_lead_time(self.frontend_com['machine_two_with_variation_lead_time'], variation_minutes)
                    temp2 = machine_two_with_variation_lead_time

            #### machine 3 production
            if (self.frontend_com['stock_level_3_with_variation'] is not 0):
                if (machine_three_with_variation_lead_time is not 0):
                    machine_three_with_variation_lead_time = machine_three_with_variation_lead_time - 1

                else:
                    self.frontend_com['stock_level_FG_with_variation'] = self.frontend_com[
                                                                           'stock_level_FG_with_variation'] + 1
                    self.frontend_com['stock_level_3_with_variation'] = self.frontend_com[
                                                                          'stock_level_3_with_variation'] - 1
                    machine_three_with_variation_lead_time = self.get_lead_time(self.frontend_com['machine_three_with_variation_lead_time'], variation_minutes)
                    temp3 = machine_three_with_variation_lead_time

            ### Progressbars
            self.frontend_com['machine_one_no_variation_status'] = int(100 - (machine_one_no_variation_lead_time / self.frontend_com['machine_no_variation_lead_time']* 100))
            self.frontend_com['machine_two_no_variation_status'] = int(
                100 - (machine_two_no_variation_lead_time / self.frontend_com['machine_no_variation_lead_time'] * 100))
            self.frontend_com['machine_three_no_variation_status'] = int(
                100 - (machine_three_no_variation_lead_time / self.frontend_com['machine_no_variation_lead_time'] * 100))
            self.frontend_com['machine_one_with_variation_status'] = int(
                100 - (machine_one_with_variation_lead_time / temp1 * 100))
            self.frontend_com['machine_two_with_variation_status'] = int(
                100 - (machine_two_with_variation_lead_time / temp2 * 100))
            self.frontend_com['machine_three_with_variation_status'] = int(
                100 - (machine_three_with_variation_lead_time / temp3 * 100))


            self.process_loop.emit(self.frontend_com)

            if self.speed is not 0:
                time.sleep(1/self.speed)
            elif self.frontend_com['iteration'] % 10000:
                time.sleep(1/10000)


    def change_speed(self,speed):
        self.speed = speed

    def get_lead_time(self, meantime, variation):
        return meantime + random.randint(0,variation*2) - variation

