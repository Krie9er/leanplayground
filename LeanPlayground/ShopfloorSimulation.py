import queue
from Exceptions import *

class Shopfloor():
    stocks = []
    def __init__(self, name, stocks=1):
        self.name = name
        for stock in range(1,stocks):
            self.stocks.append(PriorityStock())

class PriorityStock():
    items = queue.PriorityQueue()
    def __init__(self):
        self.item_id = 1

    def add_item(self, item):
        self.items.put((self.item_id, item))
        self.item_id = self.item_id + 1

    def get_item(self):
        return self.items.get()

    def get_stock_level(self):
        return self.items.qsize()

class Machine():
    def __init__(self,input_stock,output_stock):
        if not type(input_stock).__name__ == type(PriorityStock()).__name__:
            raise InvalidStockTypeError("You have passed {0} need PriorityStock".format(type(input_stock).__name__))
        self.input_stock = input_stock
        if not type(output_stock).__name__ == type(PriorityStock()).__name__:
            raise InvalidStockTypeError("You have passed {0} need PriorityStock".format(type(output_stock).__name__))
        self.output_stock = output_stock




s = PriorityStock()
print(type(s).__name__)
s.add_item("G")
s.add_item("E")
s.add_item("O")
print(s.get_stock_level())
print(s.get_item()[-1])
print(s.get_item())
print(s.get_item())
m = Machine("a","b")
