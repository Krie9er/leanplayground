import time
class Workpiece():
    production_steps = []
    def __init__(self,timestamp,name=str(time.time_ns())[-14:-6]):
        self.setNewProcess(timestamp)
        self.name = name

    def setNewProcess(self,timestamp,force=False):
        if len(self.production_steps)>0 and self.production_steps[-1].end_timestamp > 0 or len(self.production_steps) == 0:
            self.production_steps.append(ProductionStep(timestamp))
        elif force:
            self.production_steps[-1].setEnd(timestamp)
            self.production_steps.append(ProductionStep(timestamp))
        else:
            return print("Process is running, no new process started")


    def endCurrentProcess(self,timestamp):
        self.production_steps[-1].setEnd(timestamp)

    def getProcessDurations(self):
        return [step.process_duration for step in self.production_steps]

    def getWaitingDurations(self):
        waiting_durations = []
        for i in range(1,len(self.production_steps)):
            waiting_durations.append(self.production_steps[i].start_timestamp - self.production_steps[i-1].end_timestamp)
        return waiting_durations

    def getTotalWaitingDuration(self):
        return sum(self.getWaitingDurations())

    def getTotalProcessDuration(self):
        return sum(self.getProcessDurations())

    def getThroughPutTime(self):
        return self.production_steps[-1].end_timestamp - self.production_steps[0].start_timestamp

    def getValueStreamEffectiveness(self):
        return self.getTotalProcessDuration()/self.getThroughPutTime()

class ProductionStep():

    start_timestamp = 0
    end_timestamp = 0
    process_duration = 0

    def __init__(self, timestamp):
        self.start_timestamp = timestamp

    def setEnd(self, timestamp):
        self.end_timestamp = timestamp
        self.process_duration = timestamp - self.start_timestamp

def debug():
    wp = Workpiece(0)
    wp.endCurrentProcess(5)
    wp.setNewProcess(6)
    wp.endCurrentProcess(8)
    wp.setNewProcess(20)
    wp.endCurrentProcess(22)
    wp.setNewProcess(24)
    wp.endCurrentProcess(31)
    print(wp.getProcessDurations())
    print(wp.getWaitingDurations())
    print(wp.getThroughPutTime())
    print(wp.getTotalWaitingDuration())
    print(wp.getTotalProcessDuration())
    print(wp.getValueStreamEffectiveness())
    #print(int(wp.name))
    print(wp.name)
    stock = []


debug()

