import sys, os
from PyQt5 import QtWidgets, uic, QtGui
from LeanPlayground import EffectOnVariationExample, Routing
from PyQt5.QtGui import QIcon, QPixmap

class StartProgram(QtWidgets.QMainWindow):
    def __init__(self, parent=None):

        self.rootDir = os.path.dirname(__file__)
        print(os.path.join(self.rootDir, 'LeanPlayground/gui/navigatorwindow.ui'))
        super().__init__(parent)
        self.ui = uic.loadUi('LeanPlayground/gui/navigatorwindow.ui', self)
        self.ui.licenseOwnerLogo.setPixmap(QPixmap('LeanPlayground/assets/apple-icon-120x120.png'))
        self.show()
        self.routing()

    def routing(self):
        self.ui.btnEffectOfVariationExample.clicked.connect(self.playEffectOfVariationExample)

    def playEffectOfVariationExample(self):
        widget = EffectOnVariationExample.PlayEOVE(self)
        print("Test")
        self.hide()


